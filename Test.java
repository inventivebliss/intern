package com.example.internship;

import android.app.ListActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class Test extends ListActivity implements OnScrollListener {

    Photo adapter = new Photo ();
    int i,ctr,flag=1,pcount=40;
    int lastVisibleItem = 0;
    boolean loadMore,scrolldown=false;
     Context context;
    Integer [] photo = new Integer[]{
    		
    		R.drawable.srk,
            
    };
    List<Integer> newphotos = new ArrayList<Integer>(Arrays.asList(photo));
    HashMap<Integer, Integer> likes = new HashMap<Integer,Integer>();
    HashMap<Integer, Integer> comments = new HashMap<Integer,Integer>();
   
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(adapter); 
        getListView().setOnScrollListener(this);
        
    }

    @Override
	public void onScroll(AbsListView view, int firstVisible,
			int visibleCount, int totalCount) {
		// TODO Auto-generated method stub
		loadMore = firstVisible + visibleCount >= totalCount;
	        if(loadMore) {
	            adapter.count += visibleCount; 
	            adapter.notifyDataSetChanged();
	         }
	        else
	        {
	        	if(firstVisible + visibleCount == totalCount-1)
	        	{
	        		flag=0;
	        	}
	       }
    }    

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub
		
		
		
	}
	class Photo extends BaseAdapter {
        int count = 40; 
        public int getCount() { return count; }
        public Object getItem(int pos) { return pos; }
        public long getItemId(int pos) { return pos; }
        @Override
        public View getView(int pos, View v, ViewGroup p) {
        	
               int c = newphotos.size();
            if(c==1)
            {
    		for(int i=0;i<40;i++)
    		{
    			newphotos.add(R.drawable.srk2);
    			likes.put(newphotos.get(i), 34);
    			comments.put(newphotos.get(i), 45);
    		}
            }
             else
             {
            	 if(flag==0)
            	 {
            		
            	 for(int i=0;i<5;i++)
         		{
         			newphotos.add(R.drawable.srk2);
         			likes.put(newphotos.get(i), 34);
         			comments.put(newphotos.get(i), 45);
         		}
            
            	 flag=1;
            		
            	 }
            	
             }
    		    	LayoutInflater inflater=getLayoutInflater();
    		    	
                     View rowView = inflater.inflate(R.layout.listviewlayout, null);
            
                ImageView v1=(ImageView) rowView.findViewById(R.id.photo);
                v1.setImageResource(newphotos.get(pos));
                TextView v2=(TextView) rowView.findViewById(R.id.likes);
                v2.setText("likes "+Integer.toString(likes.get(newphotos.get(pos))));
                TextView v3=(TextView) rowView.findViewById(R.id.comm);
                v3.setText("Comments "+Integer.toString(comments.get(newphotos.get(pos)))+ "    " +Integer.toString(pos) + " "+ Integer.toString(newphotos.size()));
               
               
                return rowView;
        }
    }
}